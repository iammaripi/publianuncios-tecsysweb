package acciones;
 
import java.io.File;
import org.apache.commons.io.FileUtils;
import com.opensymphony.xwork2.ActionSupport;
import dao.Params;
import java.util.UUID;
 
@SuppressWarnings("serial")
public class SubirFoto extends ActionSupport{

	private File file;
	@SuppressWarnings("unused")
	private String contentType;
	private String filename;
	private String name;
 
    public String execute() {
        try {
        	String rnd = UUID.randomUUID().toString();
        	String extension = "";
			int i = filename.lastIndexOf('.');
			if (i > 0) {
			    extension = filename.substring(i);
			}
			this.name = rnd + extension;
            String path = Params.PHOTOS_PATH + this.name;
            File fileToCreate = new File(path);
            FileUtils.copyFile(this.file, fileToCreate);

        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());
 
            return INPUT;
        }
        return SUCCESS;
    }

    public String getName(){
    	return this.name;
    }
 
 	public void setUpload(File file) {
		this.file = file;
	}

	public void setUploadContentType(String contentType) {
		this.contentType = contentType;
	}

	public void setUploadFileName(String filename) {
		this.filename = filename;
	}
 
}