package websockets;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.json.JSONException;
import org.json.JSONObject;

@ServerEndpoint("/chat")
public class Chat{

	static Set<Session> users = Collections.synchronizedSet(new HashSet<Session>());

	@OnOpen 
	public void open(Session session) {
		users.add(session);
	}

	@OnClose 
	public void close(Session session) {
		users.remove(session);
	}

	@OnMessage 
	public void recibir(String msg, Session session) {
		try {
			JSONObject jso = new JSONObject(msg);
			enviar(session, jso);
		} catch (JSONException e) {
			
		}
	}

	private void enviar(Session session, JSONObject jso){
		try {
			for(Session s: users){
				s.getBasicRemote().sendText(jso.toString());
			}
		} catch (IOException e) {
			users.remove(session);
		}
	}
}