package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dominio.Anuncio;

@SuppressWarnings("unused")
public class DAOAnuncio {

	public static void insert(Anuncio anuncio) throws SQLException, Exception {
		Connection bd=Broker.get().getInserter();
		try {
			String sql="{call insertarAnuncio (?, ?, ?, ?, ?, ?, ?)}";
			CallableStatement cs=bd.prepareCall(sql);
			cs.setInt(1, anuncio.getIdAnunciante());
			cs.setString(2, anuncio.getDescripcion());
			cs.setInt(3, anuncio.getIdCategoria());
			cs.setString(4, anuncio.getPathFoto());
			cs.setFloat(5, anuncio.getPrecio());
			cs.setString(6, anuncio.getPathVideo());
			cs.registerOutParameter(7, java.sql.Types.VARCHAR);
			cs.executeUpdate();
			String exito=cs.getString(7);
			if (exito!=null && !(exito.equals("OK")))
				throw new SQLException(exito);
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			bd.close();
		}
	}
	
	

}