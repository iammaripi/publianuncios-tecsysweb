package dao;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dao.Params;

import org.apache.commons.dbcp2.BasicDataSource;

public class Broker{

    private static Broker broker;
    private BasicDataSource selector;
    private BasicDataSource inserter;

    private Broker() throws IOException, SQLException, PropertyVetoException {
        // Selector
        selector = new BasicDataSource();
        selector.setDriverClassName(Params.DRIVER);
        selector.setUsername(Params.SELECT_USER);
        selector.setPassword(Params.SELECT_PASSWORD);
        selector.setUrl(Params.URL);
        selector.setMaxTotal(Params.MAX_SELECT); // Max connections
        // Inserter
        inserter = new BasicDataSource();
        inserter.setDriverClassName(Params.DRIVER);
        inserter.setUsername(Params.INSERT_USER);
        inserter.setPassword(Params.INSERT_PASSWORD);
        inserter.setUrl(Params.URL);
        inserter.setMaxTotal(Params.MAX_INSERT); // Max connections
    }

    public static Broker get() throws IOException, SQLException, PropertyVetoException {
        if (broker== null) {
            broker= new Broker();
            return broker;
        } else {
            return broker;
        }
    }

    public Connection getSelector() throws SQLException {
        return this.selector.getConnection();
    }

    public Connection getInserter() throws SQLException {
        return this.inserter.getConnection();
    }
    
	public boolean existe(String email, String password) throws SQLException {
		boolean resultado=false;
		Connection db=getSelector();
		try{
			String SQL="Select id from usuarios where email=?";
			PreparedStatement p=db.prepareStatement(SQL);
			p.setString(1, email);
			ResultSet r=p.executeQuery();
			Connection result=null;
			if(r.next()){
				int id=r.getInt(1);
				String idUsuario="tysweb2015" + id;
				result=DriverManager.getConnection(Params.URL, idUsuario, password);
				resultado=true;
				result.close();
				r.close();
			}
			else{
				throw new SQLException ("Login o password inválidos");
			}
			return resultado;
		}
		catch(SQLException e){
			throw e;
		}
		finally{
			db.close();
		}
	}

}