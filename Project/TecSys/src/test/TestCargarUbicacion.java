package test;

import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestCargarUbicacion {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://localhost:8080/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }
  
  @Test
  public void testCargarCCAA() throws Exception {
	int i=0;
    driver.get(baseUrl + "/TecSys/");
    String[] listaCCAA={"Andaluc�a", "Arag�n", "Asturias", "Balears, Illes", "Canarias", "Cantabria", 
    		"Castilla y Le�n", "Castilla-La Mancha", "Catalu�a", "Ceuta", "Extremadura", "Galicia", 
    		"La Rioja", "Madrid", "Melilla", "Murcia", "Navarra", "Pa�s Vasco", "Valencia",};
    
    WebElement cajaCCAA=driver.findElement(By.id("ccaa"));
    Select selectCCAA = new Select(cajaCCAA);
    List<WebElement> selectOpciones = selectCCAA.getOptions();
    
    for (WebElement listaOpciones : selectOpciones) {
        assertTrue((listaOpciones.getText()).equals(listaCCAA[i]));
        i++;
    }
  }

  @Test
  public void testCargarProvincias() throws Exception {
	int i=0;
    driver.get(baseUrl + "/TecSys/");
    new Select(driver.findElement(By.id("ccaa"))).selectByVisibleText("Castilla-La Mancha");
    driver.findElement(By.cssSelector("option[value=\"8\"]")).click();
    String[] listaCastillaMancha={"Albacete", "Ciudad Real", "Cuenca", "Guadalajara", "Toledo"};
    WebElement cajaProvincias=driver.findElement(By.id("provincia"));
    Select selectProvincias = new Select(cajaProvincias);
    List<WebElement> selectOpciones = selectProvincias.getOptions();
    
    for (WebElement listaOpciones : selectOpciones) {
        assertTrue((listaOpciones.getText()).equals(listaCastillaMancha[i]));
        i++;
    }
  }

  @Test
  public void testCargarMunicipios() throws Exception {
	int i=0;
    driver.get(baseUrl + "/TecSys/");
    new Select(driver.findElement(By.id("ccaa"))).selectByVisibleText("Castilla-La Mancha");
    driver.findElement(By.cssSelector("option[value=\"8\"]")).click();
    new Select(driver.findElement(By.id("provincia"))).selectByVisibleText("Ciudad Real");
    driver.findElement(By.cssSelector("option[value=\"47\"]")).click();
    
    String[] listaCiudadReal={"Aben�jar", "Agudo", "Alamillo", "Albaladejo", "Alc�zar de San Juan", 
    		"Alcoba", "Alcolea de Calatrava", "Alcubillas", "Aldea del Rey", "Alhambra", "Almad�n", 
    		"Almadenejos", "Almagro", "Almedina", "Almod�var del Campo", "Almuradiel", "Anchuras", 
    		"Arenales de San Gregorio", "Arenas de San Juan", "Argamasilla de Alba", 
    		"Argamasilla de Calatrava", "Arroba de los Montes", "Ballesteros de Calatrava", 
    		"Bola�os de Calatrava", "Brazatortas", "Cabezarados", "Cabezarrubias del Puerto", 
    		"Calzada de Calatrava", "Campo de Criptana", "Ca�ada de Calatrava", "Caracuel de Calatrava",
    		"Carri�n de Calatrava", "Carrizosa", "Castellar de Santiago", "Chill�n", "Ciudad Real", 
    		"Corral de Calatrava", "Cortijos, Los", "C�zar", "Daimiel", "Fern�n Caballero", 
    		"Fontanarejo", "Fuencaliente", "Fuenllana", "Fuente el Fresno", "Gran�tula de Calatrava", 
    		"Guadalmez", "Herencia", "Hinojosas de Calatrava", "Horcajo de los Montes", "Labores, Las", 
    		"Llanos del Caudillo", "Luciana", "Malag�n", "Manzanares", "Membrilla", "Mestanza", 
    		"Miguelturra", "Montiel", "Moral de Calatrava", "Navalpino", "Navas de Estena", 
    		"Pedro Mu�oz", "Pic�n", "Piedrabuena", "Poblete", "Porzuna", "Pozuelo de Calatrava", 
    		"Pozuelos de Calatrava, Los", "Puebla de Don Rodrigo", "Puebla del Pr�ncipe", 
    		"Puerto L�pice", "Puertollano", "Retuerta del Bullaque", "Robledo, El", "Ruidera", 
    		"Saceruela", "San Carlos del Valle", "San Lorenzo de Calatrava", 
    		"Santa Cruz de los C��amos", "Santa Cruz de Mudela", "Socu�llamos", "Solana del Pino", 
    		"Solana, La", "Terrinches", "Tomelloso", "Torralba de Calatrava", "Torre de Juan Abad", 
    		"Torrenueva", "Valdemanco del Esteras", "Valdepe�as", "Valenzuela de Calatrava", 
    		"Villahermosa", "Villamanrique", "Villamayor de Calatrava", "Villanueva de la Fuente", 
    		"Villanueva de los Infantes", "Villanueva de San Carlos", "Villar del Pozo", 
    		"Villarrubia de los Ojos", "Villarta de San Juan", "Viso del Marqu�s"};
    
    WebElement cajaMunicipios=driver.findElement(By.id("municipio"));
    Select selectMunicipios = new Select(cajaMunicipios);
    List<WebElement> selectOpciones = selectMunicipios.getOptions();
    
    for (WebElement listaOpciones : selectOpciones) {
        assertTrue((listaOpciones.getText()).equals(listaCiudadReal[i]));
        i++;
    }
  }
  
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
