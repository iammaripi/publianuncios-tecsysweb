package dominio;

import java.sql.SQLException;
import dao.DAOAnuncio;
import dao.DAOUsuario;

@SuppressWarnings("unused")
public class Anuncio {
	private String descripcion;
	private int idCategoria;
	private int idAnunciante;
	private String pathFoto;
	private float precio;
	private String pathVideo;
	private int id;

	public Anuncio(String descripcion, int idCategoria, int idAnunciante, String pathFoto, float precio, String pathVideo) {
		this.descripcion = descripcion;
		this.idCategoria = idCategoria;
		this.idAnunciante = idAnunciante;
		this.pathFoto = pathFoto;
		this.precio = precio;
		this.pathVideo = pathVideo;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getPathVideo() {
		return pathVideo;
	}

	public void setPathVideo(String pathVideo) {
		this.pathVideo = pathVideo;
	}

	public String getPathFoto() {
		return pathFoto;
	}

	public void setPathFoto(String pathFoto) {
		this.pathFoto = pathFoto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public int getIdAnunciante() {
		return idAnunciante;
	}

	public void setIdAnunciante(int idAnunciante) {
		this.idAnunciante = idAnunciante;
	}
	
	public void setId(int id) {
		this.id=id;
	}


	public void insert() throws Exception {
		DAOAnuncio.insert(this);
	}

}
