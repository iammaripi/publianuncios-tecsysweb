<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%
	String price = request.getParameter("price");
%>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="google-signin-client_id" content="441401556244-jptfkd7ifcbcuhc4akuahsins9gk0cnq.apps.googleusercontent.com">
	<title>Publianuncios</title>
	<meta name="viewport" content="width=device-width, user-scalable=no initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<!-- LIBS -->
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/bootstrap-theme.min.css"> 
	<script type="text/javascript" src="./js/jquery.js"></script>
	<script type="text/javascript" src="./js/bootstrap.min.js"></script>
	<script type="text/javascript" src="./js/palette.js"></script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script type="text/javascript" src="https://bridge.paymill.com/"></script>
	
	<!-- OWN SCRIPTS -->
	<script type="text/javascript" src="./js/payment_form.js"></script>
	<script type="text/javascript" src="./js/common.js"></script>

	<!-- OWN STYLES -->
	<link rel="stylesheet" href="./css/style.css">
	
</head>
<body>
	<header>
		<nav class="navbar navbar-default navbar-static-top navbar-inverse navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" data-toggle="collapse" data-target="#nav1" class="navbar-toggle collapsed">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="./index.html" class="navbar-brand"><span class="glyphicon glyphicon-send"></span> Publianuncios</a>
				</div>
				<div class="collapse navbar-collapse" id="main-bar">
					<ul class="nav navbar-nav" id="bar-options">
						<li>
							<form action="" class="navbar-form navbar-left" role="Search">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Búsqueda">
								</div>
							</form>
						</li>
						<li>
							<button class="btn btn-primary navbar-btn">
								<span class="glyphicon glyphicon-search"></span>&nbsp; 
							</button>
						</li>
						<li id="payment">
							<a href='./payment_form.html'>
								<b><span class='glyphicon glyphicon-plus'> </span> Añadir anuncio</b>
							</a>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right" id="right-bar">
						<li id="chat-popup">
							<div class="dropdown">
								<button class="btn btn-info navbar-btn dropdown-toggle" id="chat-menu" aria-haspopup="true">
									<span class="glyphicon glyphicon-envelope"></span> 
									<b>Chat</b>
									<span class="caret"></span>
								</button>
								<div class="dropdown-menu" aria-labelledby="chat-menu" id="chat-box">
									<!-- CHAT -->
									<div id="chat-messages"></div>
									<form class="form-inline" id="chat-form">
										<div class="form-group">
											<input type="text" name="message" class="form-control" placeholder="Mensaje...">
											<button type="submit" class="btn btn-primary" id="send-message">
												<span class="glyphicon glyphicon-chevron-right" ></span><b>Enviar</b>
											</button>
										</div>
									</form>
								</div>
							</div>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
								<span class="glyphicon glyphicon-user"></span> Usuarios
								<span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
								<!-- ITEMS DEL USUARIO (REGISTRAR, LOGIN, ETC.) -->
								<li id="register"><a href="./register_form.html">Registrar</a></li>
								<li id="login"><a href="./login_form.html">Login</a></li>
								<li id="logout"><a href="#">Logout</a></li>
								<li><div id="google-login" class="g-signin2 navbar-btn" data-onsuccess="google_sign_in" style="margin-left:18px;"></div></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<div class="container-fluid">
		<aside class="col-md-3">
			<div class="col-md-12 thumbnail" style="background:#FFFFCC;">
				<iframe class="anuncio" src="http://localhost:8085/CentroDePublicidad/enviarAnuncio.jsp" frameborder="0" scrolling="no"></iframe>
			</div>
			<div class="col-md-12 thumbnail" style="background:#FFFFCC;">
				<iframe class="anuncio" src="http://localhost:8085/CentroDePublicidad/enviarAnuncio.jsp" frameborder="0" scrolling="no"></iframe>
			</div>
			<div class="col-md-12 thumbnail" style="background:#FFFFCC;">
				<iframe class="anuncio" src="http://localhost:8085/CentroDePublicidad/enviarAnuncio.jsp" frameborder="0" scrolling="no"></iframe>
			</div>
			<div class="col-md-12 thumbnail" style="background:#FFFFCC;">
				<iframe class="anuncio" src="http://localhost:8085/CentroDePublicidad/enviarAnuncio.jsp" frameborder="0" scrolling="no"></iframe>
			</div>
		</aside>

		<div class="col-md-9">
			<div class="row">
				<!-- BREADCRUMBS -->
				<ol class="breadcrumb">
					<li>
						<a href="./index.html" id="#index">Inicio</a>
					</li>
					<li>
						<a href="#" id="#current_category">Pago</a>
					</li>
				</ol>
			</div>
			<div class="row">
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4><span class="glyphicon glyphicon-eur"></span> <b>Pago</b></h4>
					</div>
					<div class="panel-body">
						<div class="col-md-10 col-md-offset-1">
							<label class="payment-errors"></label>
							<form id="payment-form">
								<div class="form-group">
									<label>Importe</label>
									<input class="card-amount-int form-control" type="number"readonly="readonly" value="<%= price %>" required/>
								</div>
								<div class="form-group">
									<label>Moneda</label>
									<input class="card-currency form-control" type="text" readonly="readonly" value="EUR"/ required>
								</div>
								<div class="form-row form-group">
									<label>Número de tarjeta</label>
									<input class="card-number form-control" type="text" size="20"/ required>
								</div>
								<div class="form-row form-group">
									<label>CVC</label>
									<input class="card-cvc form-control" type="text" size="3" required/>
								</div>
								<div class="form-row form-group">
									<label>Nombre del titular</label>
									<input class="card-holdername form-control" type="text" size="20" required/>
								</div>
								<div class="form-row form-group">
									<label>Fecha de caducidad (MM/AAAA)</label>
									<div class="form-inline">
										<input class="card-expiry-month form-control" type="number" max="12" size="2" required/>
										<span></span>
										<input class="card-expiry-year form-control" type="number" size="4" required/>
									</div>
								</div>
								<button class="btn btn-primary" id="btn-pay"><b>Pagar</b></button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<footer class="footer navbar-inverse">
		<div class="container-fluid text-footer">
			<div class="col-md-3">
				<a class="" href="#">
					<b>Términos de uso</b>
				</a>
			</div>
			<div class="col-md-3">
				<a class="" href="#">
					<b>Política de cookies</b>
				</a>
			</div>
			<div class="col-md-3">
				<a class="" href="#">
					<b>¿Quiénes somos?</b>
				</a>
			</div>
			<div class="col-md-3">
				<a class="" href="#">
					<b>Contacto</b>
				</a>
			</div>
		</div>
	</footer>
</body>
</html>

