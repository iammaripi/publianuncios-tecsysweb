<%@ page language="java" contentType="application/json"%>
<%@ page import= "java.sql.*,org.json.*, dao.*" %>
<%
	Connection bd = Broker.get().getSelector();
	
	String sql = "SELECT id, nombre FROM categorias ORDER BY nombre";
	PreparedStatement ps = bd.prepareStatement(sql);
	ResultSet rs = ps.executeQuery();
	JSONArray jsa = new JSONArray();
	while(rs.next()){
		JSONObject jso = new JSONObject();
		jso.put("id", rs.getInt(1));
		jso.put("nombre", rs.getString(2));
		jsa.put(jso);
	}
	
	bd.close();
%>
{"all":
	<%= jsa.toString() %>
}