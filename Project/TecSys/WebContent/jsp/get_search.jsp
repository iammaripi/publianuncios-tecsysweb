<%@ page language="java" contentType="application/json"%>
<%@ page import= "java.sql.*,org.json.*,dao.*" %>

{"all":
<%
	Connection bd = Broker.get().getSelector();
	int idLocation = Integer.parseInt(request.getParameter("idLocation"));
	
	String sql = "SELECT tipo FROM ubicaciones WHERE id=?;";
	PreparedStatement ps = bd.prepareStatement(sql);
	ps.setInt(1, idLocation);
	ResultSet rs = ps.executeQuery();
	if(rs.next()){
		switch(rs.getString(1)){
		case "Comunidad aut�noma":
			sql = "SELECT a.* FROM anuncios AS a INNER JOIN usuarios AS u ON a.idAnunciante=u.id INNER JOIN ubicaciones AS ub1 ON u.idUbicacion=ub1.id INNER JOIN ubicaciones AS ub2 ON ub1.idPadre=ub2.id WHERE ub2.idPadre=? ORDER BY a.fechaDeAlta DESC;";
			break;
		case "Provincia":
			sql = "SELECT a.* FROM anuncios AS a INNER JOIN usuarios AS u ON a.idAnunciante=u.id INNER JOIN ubicaciones AS ub ON u.idUbicacion=ub.id WHERE ub.idPadre=? ORDER BY a.fechaDeAlta DESC;";
			break;
		case "Municipio":
			sql = "SELECT a.* FROM anuncios AS a INNER JOIN usuarios AS u ON a.idAnunciante=u.id WHERE u.idUbicacion=? ORDER BY a.fechaDeAlta DESC;";
			break;
		}
		ps = bd.prepareStatement(sql);
		ps.setInt(1, idLocation);
		rs = ps.executeQuery();
		JSONArray jsa = new JSONArray();
		while(rs.next()){
			JSONObject jso = new JSONObject();
			jso.put("id",rs.getInt(1));
			jso.put("fechaDeAlta", rs.getTimestamp(2));
			jso.put("descripcion", rs.getString(3));
			jso.put("idCategoria", rs.getString(4));
			jso.put("idAnunciante", rs.getInt(5));
			jso.put("pathFoto", rs.getString(6));
			jso.put("precio", rs.getFloat(7));
	   		jso.put("pathVideo", rs.getString(8));
			jsa.put(jso);
		}
%>
	<%= jsa.toString() %>
<%
	}else{
%>
	[]
<%
	}

	
	bd.close();
%>
}