<%@ page language="java" contentType="application/json"%>
<%@ page import= "java.sql.*,org.json.*, dao.Broker" %>
<%
	Connection bd = Broker.get().getSelector();
	String type = request.getParameter("tipo");
	String parent = request.getParameter("idPadre");

	String sql = "SELECT id, nombre FROM ubicaciones WHERE tipo=? ";

	if(parent != null){
		sql += "AND idPadre=? ORDER BY nombre;";
	}else{
		sql += "ORDER BY nombre;";
	}

	PreparedStatement ps = bd.prepareStatement(sql);
	ps.setString(1, type);

	if(parent != null)
		ps.setString(2, parent);

	ResultSet rs = ps.executeQuery();
	JSONArray jsa = new JSONArray();
	while(rs.next()){
		JSONObject jso = new JSONObject();
		jso.put("id", rs.getInt(1));
		jso.put("nombre", rs.getString(2));
		jsa.put(jso);
	}
	bd.close();
%>
{"all":
	<%= jsa.toString() %>
}