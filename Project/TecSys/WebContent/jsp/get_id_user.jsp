<%@ page language="java" contentType="application/json"%>
<%@ page import= "java.sql.*,org.json.*, dao.*" %>
<%
	Connection bd = Broker.get().getSelector();
	String email = request.getParameter("email");

	String sql = "SELECT id FROM usuarios WHERE email=?";
	PreparedStatement ps = bd.prepareStatement(sql);
	ps.setString(1, email);
	ResultSet rs = ps.executeQuery();
	JSONObject jso = new JSONObject();
	if(rs.next()){
		jso.put("id", rs.getInt(1));
	}
	
	bd.close();
%>
<%= jso.toString() %>
